<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Recette</title>
</head>
<body>
<p>${recipe.name}</p>
<p>${recipe.description}</p>
<p><img id="image-1" alt="" src="${recipe.thumbnailLink}"/></p>
</body>
</html>