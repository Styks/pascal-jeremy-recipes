<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="css/normalize.css" />
<link rel="stylesheet" href="css/gallery.css" />
</head>
<body>
	<h1>Gallery</h1>

	<div class="gallery">
		<c:forEach var="item" items="${items}">
			<div class="gallery__item">
				<img class="gallery__item__image"
					src="http://lorempixel.com/200/200/food" alt="" />
				<p class="gallery__item__title">${item}</p>
			</div>
		</c:forEach>
	</div>
</body>
</html>