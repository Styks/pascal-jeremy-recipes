<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="css/normalize.css" />
<link rel="stylesheet" href="css/gallery.css" />
</head>
<body>
	<h1>Gallery</h1>

	<div class="gallery">
		<c:forEach var="recipe" items="${recipes}" varStatus="status">
			<div class="gallery__item">
				<a href="Recipe?index=${recipe.index}"> 
				<img class="gallery__item__image"
					src="${recipe.thumbnailLink}" alt="" />
					<p class="gallery__item__title">${recipe.name}</p>
				</a>
			</div>
		</c:forEach>
	</div>
</body>
</html>