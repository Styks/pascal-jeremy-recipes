package com.aiconoa.trainings.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.aiconoa.trainings.entity.Recipe;

/**
 * Servlet implementation class RecipesList
 */
@WebServlet("/RecipesList")
public class RecipesListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/DataSources/RecipeDS")
	private DataSource recipeDS;
	private static final Logger LOGGER = LogManager.getLogger();

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<Recipe> recipes = new ArrayList<>();

		try (Connection connectionBD = recipeDS.getConnection()) { // JAVA SE 7

			String sql = "SELECT * FROM recipes.recipe";
			PreparedStatement prepareStatement = connectionBD.prepareStatement(sql);
			ResultSet resultSet = prepareStatement.executeQuery();
			while (resultSet.next()) {
				String name = resultSet.getString("name");
				String thumbnailLink = resultSet.getString("thumbnailLink");
				String description = resultSet.getString("description");
				int id = resultSet.getInt("id");
				recipes.add(new Recipe(id, name, thumbnailLink, description));
			}

		} catch (SQLException e) {
			LOGGER.fatal("probleme connexion à la BDD",e);
			response.sendError(500, "Somthing wrong append");
			return;
		}

		request.setAttribute("recipes", recipes);
		// 2 passer la main a la jsp (en lui transmettant la vairible data)
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/RecipesList.jsp");
		requestDispatcher.forward(request, response);

	}

}
