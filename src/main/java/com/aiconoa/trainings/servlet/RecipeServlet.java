package com.aiconoa.trainings.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.aiconoa.trainings.entity.Recipe;

/**
 * Servlet implementation class Recipe
 */
@WebServlet("/Recipe")
public class RecipeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager.getLogger();

	@Resource(lookup = "java:jboss/DataSources/RecipeDS")
	private DataSource recipeDS;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String index = request.getParameter("index");
		Recipe recipe = null;

		try (Connection connectionBD = recipeDS.getConnection()) { // JAVA SE 7

			String sql = "SELECT * FROM recipes.recipe WHERE id=?";
			PreparedStatement prepareStatement = connectionBD.prepareStatement(sql);
			try {
				prepareStatement.setInt(1, Integer.parseInt(index));
			} catch (Exception e) {
				LOGGER.info("probleme avec parseInt",e);
				return;
			}
			ResultSet resultSet = prepareStatement.executeQuery();
			while (resultSet.next()) {
				String name = resultSet.getString("name");
				String thumbnailLink = resultSet.getString("thumbnailLink");
				String description = resultSet.getString("description");
				int id = resultSet.getInt("id");
				recipe= new Recipe(id, name, thumbnailLink, description);
			}

		} catch (SQLException e) {
			LOGGER.fatal("probleme connexion à la BDD",e);
			response.sendError(500, "Somthing wrong append");
			return;
		}
		
		

		request.setAttribute("recipe", recipe);
		// 2 passer la main a la jsp (en lui transmettant la vairible data)
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/Recipe.jsp");
		requestDispatcher.forward(request, response);
	}

}
