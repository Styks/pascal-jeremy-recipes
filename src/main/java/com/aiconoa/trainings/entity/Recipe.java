package com.aiconoa.trainings.entity;

public class Recipe {
	
	private String name;
	private String thumbnailLink;
	private String description;
	private int index;
	
    public Recipe(int index,String name, String thumbnailLink,String description) {
		super();
		this.index=index;
		this.name=name;
		this.thumbnailLink=thumbnailLink;
		this.description=description;
	}

	public String getName() {
		return name;
	}


	public String getThumbnailLink() {
		return thumbnailLink;
	}

	public String getDescription() {
		return description;
	}

	public int getIndex() {
		return index;
	}


	

}
